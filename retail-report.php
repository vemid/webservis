<?php

include('servis/nbs.php');

//echo get_middle_course();

if($_SERVER['REQUEST_METHOD'] === 'POST') {

    $dateHash = date("d-m-Y");
    $hash_decode = md5('report_' . $dateHash);

    $data = $_POST;


    if ($data["hash"] == $hash_decode) {

        //echo "usao";
        //echo get_middle_course();
        $middleCourse = get_middle_course();

        $hashSystemData = $data["system"];
        $hashSystemDecode = md5('bebakids');

        putenv("INFORMIXDIR=/opt/informix");
        putenv("CLIENT_LOCALE=CS_CZ.CP1250");
        putenv("DB_LOCALE=CS_CZ.CP1250");
        putenv("DBLANG=CS_CZ.CP1250");
        putenv("ODBCINI=/opt/informix/etc/odbc.ini");


        $reportDate ="";
        $data["reportDate"] ? $reportDate = $data["reportDate"] : $reportDate =date("d.m.Y");
        $ddate = date("Y-m-d",strtotime($reportDate));
        $date = new \DateTime($ddate);
        $day = $date->format("l");
        $year = $date->format("Y")-1;
        $string = "next ".$day." ".$year."-".$date->format('m-d');
        $preDatum = date ('d.m.Y', strtotime($string));

        $responseArray=array_fill_keys(
            array('turnover', 'product'), '');

        if($data["system"] === md5('bebakids')) {

            $nburl = "http://www.bebakids.com/webservis/order/orders-json.php?secKey=123456789&user=bebakids&pass=qGT6KXyqRr0TCZ3P&dateCreate=".$ddate;
            $json = file_get_contents($nburl);
            $obj = json_decode($json,true);
            $totalWeb2 = 0;
            $webPazar = 0;
            if (count($obj) > 0) {
                foreach ($obj as $data) {

                    $totalWeb2 = $totalWeb2 + $data['totalForProduct'];
                }
                $webPazar = round($totalWeb2 / $middleCourse, 1);
            }

            $nburl2 = "http://www.bebakids.com/webservis/order/orders-json.php?secKey=123456789&user=bebakids&pass=qGT6KXyqRr0TCZ3P&dateCreate=".date("Y-m-d",strtotime($preDatum));
            $json2 = file_get_contents($nburl2);
            $obj2 = json_decode($json2,true);
            $totalWebPre = 0;
            foreach ($obj2 as $dataOld) {

                $totalWebPre = $totalWebPre+$dataOld['totalForProduct'];
            }
            $webPazarPre= round($totalWebPre/$middleCourse,1);

            $webArray = [
                "SIFRA" => 'WEB',
                "OBJEKAT" => 'WEB',
                "PAZAR" => $webPazar,
                "PRE_PAZAR" => $webPazarPre,
                "BRRACUNA" => 0,
                "PRE_BRRACUNA" => 0,
                "KOLICINA" => 0,
                "PRE_KOLICINA" => 0,
            ];

            $db = new PDO("informix:DSN=misystem", "mis2open", "Psw4mis");
            $procedure = $db->prepare("execute procedure servis_retail_report('$reportDate','$preDatum','$middleCourse')");
            $stm = $db->query("select t1.objekat sifra,trim(t1.naz_obj_mp) objekat,
round(cast(t1.pazar as decimal),0) as pazar,round(t2.pazar,0) pre_pazar,
round(t1.brracuna,0) brracuna,round(t2.brracuna,0) pre_brracuna,
round(t1.kolicina,0) kolicina,round(t2.kolicina,0) pre_kolicina,
round(t1.nabavna,0) nabavna ,round(t2.nabavna,0) pre_nabavna from t_paz t1
        left join t_paz_pre t2 on t2.objekat = t1.objekat where t1.objekat not in ('WEB','WCG') order by sifra");
        }
        elseif ($data["system"] === md5('watch')) {
            $db = new PDO("informix:DSN=watch", "mis2open", "Psw4mis");
            $procedure = $db->prepare("execute procedure servis_retail_report('$reportDate','$preDatum','$middleCourse')");
            $stm = $db->query("select t1.objekat sifra,trim(t1.naz_obj_mp) objekat,
round(cast(t1.pazar as decimal),0) as pazar,round(t2.pazar,0) pre_pazar,
round(t1.brracuna,0) brracuna,round(t2.brracuna,0) pre_brracuna,
round(t1.kolicina,0) kolicina,round(t2.kolicina,0) pre_kolicina,
round(t1.nabavna,0) nabavna ,round(t2.nabavna,0) pre_nabavna from t_paz t1
        left join t_paz_pre t2 on t2.objekat = t1.objekat where t1.objekat not in ('WCG') order by sifra");
        }

        elseif ($data["system"] === md5('geox')) {
            $db = new PDO("informix:DSN=geox", "mis2open", "Psw4mis");
            $procedure = $db->prepare("execute procedure servis_retail_report('$reportDate','$preDatum','$middleCourse')");
            $stm = $db->query("select t1.objekat sifra,trim(t1.naz_obj_mp) objekat,
round(cast(t1.pazar as decimal),0) as pazar,round(t2.pazar,0) pre_pazar,
round(t1.brracuna,0) brracuna,round(t2.brracuna,0) pre_brracuna,
round(t1.kolicina,0) kolicina,round(t2.kolicina,0) pre_kolicina,
round(t1.nabavna,0) nabavna ,round(t2.nabavna,0) pre_nabavna from t_paz t1
        left join t_paz_pre t2 on t2.objekat = t1.objekat where t1.objekat not in ('WCG') order by sifra");
        }


        $sucess = $procedure->execute();
        if ($sucess) {


            $stm2 = $db->query("select first 1 t.sif_rob,r.kat_bro,trim(r.naz_Rob) naziv,round(sum(pazar*0.8333-nabavna),0) ruc,sum(kolicina) kolicina from t_paz_sum t
left join roba r on r.sif_rob = t.sif_rob
where r.sif_rob <> 'KESA'
group by 1,2
order by sum(pazar-nabavna) desc
");

            $stm3 = $db->query("select 'Sarabanda' brend,sum(kolic) kolicina, round(sum(kolic*pro_cen_bp)/117.56,2) vrednost from kasa_blok_st k
left join roba_klas_robe rkr on rkr.sif_rob = k.sif_rob and rkr.sif_kri_kla = '01'
where rkr.kla_ozn = 'T177' and k.datum = '$reportDate'
");

            $result = $stm->fetchAll(\PDO::FETCH_ASSOC);
            $result2 = $stm2->fetchAll(\PDO::FETCH_ASSOC);
            $result3 = $stm3->fetchAll(\PDO::FETCH_ASSOC);


            foreach ($result as &$item) {
                $item['OBJEKAT'] = iconv("CP1250", "UTF-8", $item['OBJEKAT']);
            }
            foreach ($result2 as &$item) {
                $item['NAZIV'] = iconv("CP1250", "UTF-8", $item['NAZIV']);
            }
        }

        if($result) {
            header('Content-type: application/json');
            if($hashSystemData == $hashSystemDecode) {
                array_push($result,$webArray);
            }
  //          array_push($result,$webArray);
            array_walk($result, function (&$value) {
                if (ctype_digit($value)) {
                    $value = (int)$value;
                }
            });


            $responseArray['turnover']=$result;
            $responseArray['product']=$result2;
            $responseArray['brend']=$result3;


            echo json_encode($responseArray);
        }
        else echo "nije dobar validator";
    }
}