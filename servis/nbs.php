<?php
/**  */

function get_middle_course()
{
    require_once(__DIR__ . "/../lib/nusoap.php");
    $login_username = 'bebakids';
    $login_password = 'adminabc123';
    $login_licenceid = '44d51ffc-4b6d-4669-87c4-78831972f1a4';

    $datenbs = date("Ymd", strtotime("-3 days"));
    $m_list_type = 0;

    $soap_wsdl = "https://webservices.nbs.rs/CommunicationOfficeService1_0/ExchangeRateXmlService.asmx?WSDL";
    $soap_namespace = "http://communicationoffice.nbs.rs";

    $soap_client = new \nusoap_client($soap_wsdl, true);

    $soap_client->decode_utf8 = 0;
    $m_header =
        '<AuthenticationHeader xmlns="' . $soap_namespace . '">
    <UserName>' . $login_username . '</UserName>
    <Password>' . $login_password . '</Password>
    <LicenceID>' . $login_licenceid . '</LicenceID>
    </AuthenticationHeader>';

    $soap_client->setHeaders($m_header);

    $parameters = array(
        'currencyCode' => 978,
        'dateFrom' => $datenbs,
        'dateTo' => $datenbs,
        //'typeID' => $m_list_type
        'exchangeRateListTypeID' => 1
    );
    $m_soap_result = $soap_client->call('GetExchangeRateByCurrency', $parameters);
    $xml = simplexml_load_string($m_soap_result['GetExchangeRateByCurrencyResult']);
    $json = json_encode($xml);
    $array = json_decode($json, TRUE);

    $BuyingRate = $array['ExchangeRate']['BuyingRate'];
    $SellingRate = $array['ExchangeRate']['SellingRate'];

    $middleCourse = ((float)$BuyingRate + (float)$SellingRate) / 2;

    return $middleCourse;
}

