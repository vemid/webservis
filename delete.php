<?php

if($_SERVER['REQUEST_METHOD'] === 'POST') {

    $data = $_POST = json_decode(file_get_contents('php://input'), true);

    $date = date("Y-m-d");
    $hash_decode = md5('bkids_prijava_' . $date);

    /* 4cb650b74d17c3855251ebaca17f8259 */
    if ($data['hash'] === $hash_decode) {

        $code = $data['code'];

        $configs = include('local.php');
        $location = $data['location'];
        $locations = array_keys((array)$configs->{'location'});

        if (in_array($location, $locations)) {

            $stations = $configs->location->$location;

            foreach ($stations as $db) {

                $mysqli = new mysqli($db->host, $db->username, $db->password, $db->database) or die('Cannot connect to the DB');

                /* check connection */
                if ($mysqli->connect_errno) {
                    printf("Connect failed: %s\n", $mysqli->connect_error);
                    exit();
                }

                $query = "select sifra from radnici WHERE sifra = $code";
                $result = $mysqli->query($query) or die('Errant query:  ' . $query);

                if ($result->num_rows != 1) {
                    header('Content-type: application/json');
                    echo json_encode(array('response' => 'code doesn\'t exists'));
                    exit();
                } else {
                    $stmt = $mysqli->query("delete from radnici WHERE sifra = $code");
                }

                $mysqli->close();

            }

            header('Content-type: application/json');
            echo json_encode(array('response' => 'user successfully deleted'));
        }
    }
}
elseif (isset($_POST['readme'])) {

    $result = array();
    $result['param1'] = 'Param 1 is hash code';
    $result['param2'] = 'Param 2 is location';
    $result['param3'] = 'Param 3 is user code';
    header('Content-type: application/json');
    echo json_encode(array('response' => $result));
    exit();
}
?>