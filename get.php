<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST['readme'])) {
        echo "usao3";
        $result = array();
        $result['hash'] = 'Param 1 is hash code';
        $result['format'] = 'Param 2 is format';
        $result['date'] = 'Param 3 is date';
        $result['location'] = 'Param 4 is location';
        header('Content-type: application/json');
        echo json_encode(array('response' => $result));
        exit();
    } else {

        if (!$data = $_POST = json_decode(file_get_contents('php://input'), true)) {
            $data = (array)$_REQUEST;
        }
        $date = date("Y-m-d");
        $hash_decode = md5('bkids_prijava_' . $date);

        /* 4cb650b74d17c3855251ebaca17f8259 */
        if ($data['hash'] === $hash_decode) {

            $date = "'" . ($data['date']) . "'";
            $format = strtolower($data['format']) == 'xml' ? 'xml' : 'json'; //xml is the default

            $configs = include('local.php');
            $location = $data['location'];
            $locations = array_keys((array)$configs->{'location'});

            if (in_array($location, $locations)) {

                $stations = $configs->location->$location;
                $posts = array();
                foreach ($stations as $db) {

                    $mysqli = new mysqli($db->host, $db->username, $db->password, $db->database) or die('Cannot connect to the DB');

                    /* check connection */
                    if ($mysqli->connect_errno) {
                        //printf("Connect failed: %s\n", $mysqli->connect_error);
                    } else {


                        $query = "select p.id,p.sifra,p.date,time(p.check_in) check_in,time(ifnull(p.check_out,time('00:00:00'))) check_out from prijava p left join radnici r on r.sifra = p.sifra WHERE p.date = $date ORDER BY ID DESC ";
                        $result = $mysqli->query($query) or die('Errant query:  ' . $query);


                        if ($result) {
                            while ($post = $result->fetch_assoc()) {
                                $posts[] = array('data' => $post);
                            }
                        }
                    }
                }


                /* output in necessary format */
                if ($format == 'json') {
                    header('Content-type: application/json');
                    $json = json_encode(array('datas' => $posts));
                    echo $json;
                } else {
                    header('Content-type: text/xml');
                    echo '<posts>';
                    foreach ($posts as $index => $post) {
                        if (is_array($post)) {
                            foreach ($post as $key => $value) {
                                echo '<', $key, '>';
                                if (is_array($value)) {
                                    foreach ($value as $tag => $val) {
                                        echo '<', $tag, '>', htmlentities($val), '</', $tag, '>';
                                    }
                                }
                                echo '</', $key, '>';
                            }
                        }
                    }
                    echo '</posts>';
                }
            } else {
                header('Content-type: application/json');
                echo json_encode(array('response' => 'location doesnt exists'));
                exit();
            }
        }
    }
}
?>