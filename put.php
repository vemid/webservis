<?php
/* require the user as the parameter */


if($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (!$data = $_POST = json_decode(file_get_contents('php://input'), true)) {
        $data = (array)$_REQUEST;
    }

    $date = date("Y-m-d");
    $hash_decode = md5('bkids_prijava_' . $date);

    /* 4cb650b74d17c3855251ebaca17f8259 */
    if ($data['hash'] === $hash_decode) {

    	$name = $data['name'];
    	$code = $data['code'];


        $configs = include('local.php');
        $location = $_POST['location'];
        $locations = array_keys((array)$configs->{'location'});

        if (in_array($location, $locations)) {

            $stations = $configs->location->$location;

            foreach ($stations as $db) {
                $mysqli = new mysqli($db->host, $db->username, $db->password, $db->database) or die('Cannot connect to the DB');

                /* check connection */
                if ($mysqli->connect_errno) {
                    printf("Connect failed: %s\n", $mysqli->connect_error);
                    exit();
                }

                $query = "select sifra from radnici  WHERE sifra = '$code'";
                $result = $mysqli->query($query) or die('Errant query:  ' . $query);

                if($result->num_rows > 0)
                {
                    header('Content-type: application/json');
                    echo json_encode(array('response' => 'code alredy exists'));
                    exit();
                }

                else {

                    $stmt = $mysqli->query("INSERT INTO radnici (ime_i_prezime, sifra) VALUES ('$name','$code')");
                }

                $mysqli->close();

            }

            header('Content-type: application/json');
            echo json_encode(array('response' => 'user successfully created'));

        }

    }
}
elseif (isset($_POST['readme'])) {

    $result = array();
    $result['hash'] = 'Param 1 is hash code';
    $result['location'] = 'Param 2 is location';
    $result['code'] = 'Param 3 is user code';
    $result['name'] = 'Param 4 is user name';
    header('Content-type: application/json');
    echo json_encode(array('response' => $result));
    exit();
}
?>